-- 10-02-2024 @author Swarg
-- Goal: to provide the ability to subscribe to events in certain instances.
-- With the ability to access the object itself and additional event-data inside
-- the subscribed handler.
-- Event bus to expand the capabilities of both:
--
-- Case 1(unique): specific certain instances  (handler for a unique instance)
-- (through creating a handler subscription for only one specific instance)
-- Case 2(common): subscription to events in all instances of a particular class
--
--
-- Case 1 (handler for a unique instance):
-- This is used to be able to create unique behavior for a specific instance.
-- To expand the capabilities of sepecific instances, each instance has its own
-- section(in bus) to which several handlers can be subscribed.
-- Inside the section, events are divided by channel names(event-names).
-- And inside each channel there can be several event listeners(handlers)
-- for that particular instance.
--
-- WARNING: it is important to clear handlers, subscribed to a specific instance
-- because in this case the instances themselves are keys in the sections table.
-- And if you do not delete this entries(keys) from the sections table, the
-- garbage collector will not be able to clean up memory from objects that
-- are no longer used.
-- Therefore, for such cases, a mechanic for automatically unsubscribing the
-- handler is provided. To do this, in the method of the object that closes the
-- object to free resources from it, you need to place the code for
-- unsubscribing to the events of this object.
--
-- Why?
-- Case 1:
-- Here the objects themselves are used as keys in the sections table to provide
-- the ability to subscribe their own unique handlers for each certain object.
-- This allows you to expand the functionality by creating unique behavior only
-- for specific instances of the same class.
-- This will allow you to subscribe handlers (listeners) only to specific objects,
-- without the need to add them as fields to the table (internal state) of the
-- object itself.
-- That is, such a bus usage will be an overhead for cases when it is necessary
-- to process events of the same class in the methods of all instances of a
-- particular class(Case-2). Instead of assigning unique handlers only to
-- certain specific objects(instances of the classes).
--
-- Case 2:
-- More common behavior of the bus usage when
-- persistent event handlers are created for all class instances.
--
--
-- Features of work:
--
-- Case-2 + Case-1 features of collaboration:
-- the joint operation of two event subscription modes is supported both for
-- unique objects and for all instances of classes.
-- When an event is triggered, the presence of a handler for a specific object
-- is first checked, and if it is not there, then the handler for all instances
-- is checked.
-- Thus, handlers for a unique object override handlers for all instances.
-- and if a handler is assigned to a specific instance, only that one will be
-- called, even if there are assigned handlers for all instances of this class.


local M = {}
local E = {}

M._VERSION = "alib.eventbus v0.1.0"
M._URL = "https://gitlab.com/lua_rocks/eventbus"

-- create new bus
function M.new()
  local bus = {} -- future object with methods

  local sections = {
  --[[
    instance1 = {                -- instances or section-name
      channel-name1 = {          -- event-name
        handler1 = userdata,
        handler2 = true,
      }
      channel-name2 = {
        handler = true ..
      }
    }
    instance2 = { -- instances
      channel-name1 = {
        handler1 = userdata,
        handler2 = true,
      }
    }
    '*' = {                      -- Case2: default handlers for all instances
      channel-name1 = {
        handlerN = true,         -- can be overrided by instanceN.channel
      }
    }
  ]]
  }

  --
  --
  --
  ---@param instance any instance of object or section name
  ---@param channel string the channel name in sections of instance
  function bus.fire_event(instance, channel, ...)
    local section = sections[instance or false] or sections['*']
    local handlers = (section or E)[channel or false]
    if handlers then
      local t = type(handlers)
      if t == 'table' then
        -- does not guarantee the same order as during registration
        for handler, userdata in pairs(handlers) do -- no order
          -- assert(type(handler) == 'function', 'handler')
          if userdata == true then
            handler(instance, ...)           -- just a function
          else
            handler(userdata, instance, ...) -- method of the object
          end
        end
        -- elseif t == 'function' then -- one handler to one channel
        --   handlers(instance, ...)
      end
    end
  end

  ---
  --- subscriber handler to an event in specified instance(object) or section
  ---
  ---@param instance table|string - unique object or section-name
  ---@param channel string        - event name
  ---@param handler function{[userdata], instance:table, ...}
  ---@param userdata table|true?
  function bus.subscribe(instance, channel, handler, userdata)
    instance = assert(instance, 'instance or section name')
    channel = assert(channel, 'channel name')
    assert(type(handler) == 'function', 'handler must be a function')

    sections[instance] = sections[instance] or {}
    local section = sections[instance]
    section[channel] = section[channel] or {}
    local t = section[channel]
    t[handler] = userdata or true
  end

  --
  -- Unsubscribe one specific handler
  -- with automatic deletion of the entire subtree if this handler was the only
  -- one for both the channel and the section.
  --
  ---@param instance table|string - section-name(or instance)
  ---@param channel string?       - event-name
  ---@param handler function|string  use * for remove all handlers from channel
  function bus.unsubscribe(instance, channel, handler)
    instance = assert(instance, 'instance or section name')
    channel = assert(channel, 'channel name')
    local t = (sections[instance] or E)[channel]
    local success = false
    if t then
      if handler == '*' then
        sections[instance][channel] = nil
        success = true
      elseif handler then
        t[handler] = nil
        success = true
      end
      -- autoclean up all subtree if was deleted the last entry
      if not next(t) then
        sections[instance][channel] = nil
        if not next(sections[instance]) then
          sections[instance] = nil
        end
      end
    end
    return success
  end

  --
  -- Remove all subscribed handlers for given instance (or section name)
  -- useful for a handler that is assigned to a method that marks an object
  -- as expired. that is, which must be removed from memory
  --
  ---@param instance table|string
  function bus.free(instance)
    assert(instance, 'instance - or section name')
    sections[instance] = nil
  end

  --
  -- for testing
  --
  function bus._get_listeners()
    return sections
  end

  --
  -- cleanup specified channel or instance(section) if given or
  -- all data if instance and channel not specitied
  --
  ---@param instance table?
  ---@param channel string?
  function bus._cleanup(instance, channel)
    if instance then
      assert(sections[instance], 'has instance')
      if channel then
        assert(sections[instance][channel], 'has channel')
        sections[instance][channel] = nil
      else
        sections[instance] = nil
      end
    else
      sections = {} -- clear all subscribers
    end
  end

  return bus
end

return M
