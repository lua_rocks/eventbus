## Event Bus

  A library for providing an Event Bus with Sections and Channels

An event bus to implement the ability to subscribe both to events occurring in
all instances of classes(or named events), and separately for specific unique
instances. Create a subscription, when notifying subscribers,
the handler is given both access to the object that caused the event and
to additional parameters(event-data) from the event emitter context.
The main purpose is the ability to expand the standard behavior of objects due
to the ability to attach handlers to specific instances of objects.
But also with the ability to implement event subscriptions for all instances.


Goal: to provide the ability to subscribe to events in certain instances.
With the ability to access the object itself and additional event-data inside
the subscribed handler.

Event bus to expand the capabilities of both:

- Case 1(unique): specific certain instances  (handler for a unique instance)
(through creating a handler subscription for only one specific instance)
- Case 2(common): subscription to events in all instances of a particular class


## Case 1 (handler for a unique instance):

This is used to be able to create unique behavior for a specific instance.
To expand the capabilities of specific instances, each instance has its own
section(in bus) to which several handlers can be subscribed.
Inside the section, events are divided by channel names(event-names).
And inside each channel there can be several event listeners(handlers)
for that particular instance.

WARNING: it is important to clear handlers, subscribed to a specific instance
because in this case the instances themselves are keys in the sections table.
And if you do not delete this entries(keys) from the sections table, the
garbage collector will not be able to clean up memory from objects that
are no longer used.
Therefore, for such cases, a mechanic for automatically unsubscribing the
handler is provided. To do this, in the method of the object that closes the
object to free resources from it, you need to place the code for
unsubscribing to the events of this object.


## Why?

Case 1:
Here the objects themselves are used as keys in the sections table to provide
the ability to subscribe their own unique handlers for each certain object.
This allows you to expand the functionality by creating unique behavior only
for specific instances of the same class.
This will allow you to subscribe handlers (listeners) only to specific objects,
without the need to add them as fields to the table (internal state) of the
object itself.
That is, such a bus usage will be an overhead for cases when it is necessary
to process events of the same class in the methods of all instances of a
particular class(Case-2). Instead of assigning unique handlers only to
certain specific objects(instances of the classes).

Case 2:
More common behavior of the bus usage when
persistent event handlers are created for all class instances.


## Features of work:

Case-2 + Case-1 features of collaboration:
the joint operation of two event subscription modes is supported both for
unique objects and for all instances of classes.
When an event is triggered, the presence of a handler for a specific object
is first checked, and if it is not there, then the handler for all instances
is checked.
Thus, handlers for a unique object override handlers for all instances.
and if a handler is assigned to a specific instance, only that one will be
called, even if there are assigned handlers for all instances of this class.


## Installation

To install the latest release

```sh
git clone --depth 1 https://gitlab.com/lua_rocks/eventbus
cd alogger
luarocks make
```

or

```sh
luarocks install https://gitlab.com/lua_rocks/eventbus/-/raw/master/alib.eventbus-scm-1.rockspec?ref_type=heads
```

With LuaRocks:
```sh
luarocks install alib.eventbus
```


## Examples

```lua
local eventbus = require 'alib.eventbus'
local bus = eventbus.new()

local M = {} -- module with handlers

M.handler_on_close = function(obj, ...)
  print('handler_on_close obj.id:', (obj or {}).id, ...)
  -- here workload on_close event
  -- to automatically unsubscribe yourself:
  bus.unsubscribe(obj, 'close', M.handler_on_close)
end

--[[
function MyClass:close(...)
  self._open = false
  bus.fire_event(self, 'close', ...)  << emit event inside instance of MyClass
end ]]
local instance = MyClass:new({ id = 1 }) -- create a new object from a class

bus.subscribe(instance_1, 'close', M.handler_on_close, true)

instance:close() -- the handler_on_close will be called inside
```

Executable examples:
[See also](./doc/examples.lua)

```sh
# from root of the project
lua doc/examples.lua N
```
where `N` is a number from 1 to 3 (variant)


## Copyright

See [MIT LICENSE file](./LICENSE)
