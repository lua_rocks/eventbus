install:
	sudo luarocks make

VERSION := "0.1.0-1"
PACKAGE := "alib.eventbus"

upload:
	luarocks upload rockspecs/$(PACKAGE)-$(VERSION).rockspec

