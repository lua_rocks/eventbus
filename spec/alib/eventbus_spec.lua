-- 10-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("alib.eventbus");


describe("lib.eventbus", function()
  it("new bus + subscribe hander to unique instance", function()
    local bus = M.new()
    local instance = {} -- Object for whose events we subscribe
    local res = {}

    ---@diagnostic disable-next-line: unused-local
    local handler = function(instance0, event)
      res[#res + 1] = 'msg-from-handler key:' .. tostring(event.key)
    end

    bus.subscribe(instance, 'channel-name', handler)

    -- before work:
    local exp = {
      [instance] = {
        ['channel-name'] = {
          [handler] = true,
        }
      }
    }
    assert.same(exp, bus._get_listeners())

    local event = { key = 1 }
    bus.fire_event(instance, 'channel-name', event) -- workload
    bus.fire_event(instance, 'channel-name', { key = 2 })

    local exp2 = { 'msg-from-handler key:1', 'msg-from-handler key:2' }
    assert.same(exp2, res)

    bus.unsubscribe(instance, 'channel-name', handler)
    assert.same({}, bus._get_listeners())
  end)


  it("unsubscribe + autoclen channel and section", function()
    local bus = M.new()
    local handler1 = function() end
    local handler2 = function() end
    local handler3 = function() end
    local instance = {}
    bus.subscribe(instance, 'event-a', handler1)
    bus.subscribe(instance, 'event-a', handler2)
    bus.subscribe(instance, 'event-b', handler3)

    local exp = {
      [instance] = {
        ['event-a'] = {
          [handler1] = true,
          [handler2] = true,
        },
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp, bus._get_listeners())

    bus.unsubscribe(instance, 'event-a', handler2)
    local exp2 = {
      [instance] = {
        ['event-a'] = { [handler1] = true, },
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp2, bus._get_listeners())

    bus.unsubscribe(instance, 'event-a', handler1)
    local exp3 = {
      [instance] = {
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp3, bus._get_listeners())

    bus.unsubscribe(instance, 'event-b', handler3)
    assert.same({}, bus._get_listeners())
  end)


  it("free (unsubscribe all handlers for specific instance(section-name)", function()
    local bus = M.new()
    local handler1 = function() end
    local handler2 = function() end
    local handler3 = function() end
    local instance = {}
    bus.subscribe(instance, 'event-a', handler1)
    bus.subscribe(instance, 'event-a', handler2)
    bus.subscribe(instance, 'event-b', handler3)

    local exp = {
      [instance] = {
        ['event-a'] = { [handler1] = true, [handler2] = true, },
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp, bus._get_listeners())

    bus.free(instance)
    assert.same({}, bus._get_listeners())
  end)


  it("free (unsubscribe all handlers for specific instance(section-name)", function()
    local bus = M.new()
    local handler1 = function() end
    local handler2 = function() end
    local handler3 = function() end
    local instance = {}
    bus.subscribe(instance, 'event-a', handler1)
    bus.subscribe(instance, 'event-a', handler2)
    bus.subscribe(instance, 'event-b', handler3)

    local exp = {
      [instance] = {
        ['event-a'] = { [handler1] = true, [handler2] = true, },
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp, bus._get_listeners())

    bus.unsubscribe(instance, 'event-a', '*')
    local exp2 = {
      [instance] = {
        ['event-b'] = { [handler3] = true, }
      }
    }
    assert.same(exp2, bus._get_listeners())
  end)


  -- one shoot handler with auto cleanup self from bus
  it("subscribe with autounsubscribe", function()
    local bus = M.new()
    local storage = {}

    local instance = {
      field = 16,
      close = function(self)
        local event = nil -- {...}
        bus.fire_event(self, 'close', event)
      end
    }

    -- put instance to a storage
    storage[instance] = true

    local mod = {}
    local res = {}

    ---@diagnostic disable-next-line: unused-local
    mod.handler = function(instance0, event)
      res[#res + 1] = 'filed:' .. tostring(instance0.field) -- report
      storage[instance0] = nil                              -- cleanup obj
      bus.unsubscribe(instance0, 'close', mod.handler)      -- auto cleanup
    end

    bus.subscribe(instance, 'close', mod.handler, true)

    -- before work:
    local exp = {
      [instance] = { ['close'] = { [mod.handler] = true } }
    }
    assert.same(exp, bus._get_listeners())
    assert.same(true, storage[instance])
    assert.same({}, res)

    instance:close() -- workload

    -- after work:
    assert.same({ 'filed:16' }, res)      -- result from handler

    assert.same({}, bus._get_listeners()) -- auto cleaned
    assert.same(nil, storage[instance])
  end)


  -- two one shoot handlers with auto cleanup self from bus
  -- ensure no conflict in same channel in bus
  it("subscribe with autounsubscribe two handler some channel", function()
    local bus = M.new()
    local instance = {}
    local mod = {}

    ---@diagnostic disable-next-line: unused-local
    mod.handler_a = function(instance0, event)
      -- here workload
      -- remove self from bus
      bus.unsubscribe(instance0, 'close', mod.handler_a)
    end

    local userdata = {}
    ---@diagnostic disable-next-line: unused-local
    mod.handler_b = function(self, instance0, event)
      self.passed = true
      bus.unsubscribe(instance0, 'close', mod.handler_b)
    end

    -- one channel two handlers
    bus.subscribe(instance, 'close', mod.handler_a, true)     -- function
    bus.subscribe(instance, 'close', mod.handler_a, true)     -- override self!
    bus.subscribe(instance, 'close', mod.handler_b, userdata) -- method

    -- before work:
    local exp = {
      [instance] = {
        ['close'] = {
          [mod.handler_a] = true,
          [mod.handler_b] = userdata,
        }
      }
    }
    assert.same(exp, bus._get_listeners())

    -- fire_event  somewhere inside the instance
    mod.handler_a(instance, nil)

    -- after work:
    local exp2 = {
      [instance] = {
        ['close'] = {
          [mod.handler_b] = userdata,
        }
      }
    }
    assert.same(exp2, bus._get_listeners()) -- auto cleaned

    -- fire_event  somewhere inside the instance
    mod.handler_b(userdata, instance, nil)
    assert.same({}, bus._get_listeners())
    assert.same(true, userdata.passed)
  end)


  -- two one shoot handlers with auto cleanup self from bus
  -- ensure no conflict in same channel in bus
  it("subscribe with autounsubscribe", function()
    local bus = M.new()
    local instance, mod, res = {}, {}, {}

    ---@diagnostic disable-next-line: unused-local
    mod.handler_a = function(instance0, event)
      res[#res + 1] = 'msg-from-handler-a'
      bus.unsubscribe(instance0, 'close', mod.handler_a)
    end

    local userdata = {}
    ---@diagnostic disable-next-line: unused-local
    mod.handler_b = function(self, instance0, event)
      self.passed = true
      res[#res + 1] = 'msg-from-handler-b'
      bus.unsubscribe(instance0, 'close', mod.handler_b)
    end

    -- one channel two handlers
    bus.subscribe(instance, 'close', mod.handler_a, true)
    bus.subscribe(instance, 'close', mod.handler_b, userdata)

    -- before work:
    local exp = {
      [instance] = {
        ['close'] = {
          [mod.handler_a] = true,
          [mod.handler_b] = userdata,
        }
      }
    }
    assert.same(exp, bus._get_listeners())

    bus.fire_event(instance, 'close', nil) -- workload

    -- after work:
    assert.same({}, bus._get_listeners())

    assert.same(true, userdata.passed)
    local exp2 = { 'msg-from-handler-a', 'msg-from-handler-b' }
    assert.same(exp2, res)
  end)
end)


-- Case1 and Case2

describe("lib.eventbus", function()
  local bus = M.new()

  after_each(function() bus._cleanup() end)

  -----------------------------------------------------------------------
  -- this is a Class in one method in which handlers are called for the
  -- specified event name( here  events names is `open` and `close`

  --- simple Class for testing
  ---@class oopexample.Object
  local MyClass = {
    _cname = 'oop.example.MyClass',
    _open = nil,
  }

  --- Constructor
  function MyClass:new(cdef) -- function Object.new(Object, o)
    cdef = cdef or {}
    self.__index = self      -- Object.__index = Object
    setmetatable(cdef, self)
    return cdef
  end

  function MyClass:open()
    self._open = true
    bus.fire_event(self, 'open') -- << call subscribed handlers
    --                    ^ event name
  end

  function MyClass:close(...)
    self._open = false
    bus.fire_event(self, 'close', ...) -- << call subscribed handlers
    --                             ^ event data for handler
  end

  -----------------------------------------------------------------------

  -- here about how to create a new instance of MyClass  described above
  it("minimalistic oop tooling", function()
    local cdef = {}                    -- class definition(for inheritance)
    local instance = MyClass:new(cdef) -- create new Instance of Object class
    assert.equal(cdef, instance)       -- inner state of this instance

    local classOfInstance = getmetatable(instance)
    assert.equal(MyClass, classOfInstance)
    local classname = rawget(MyClass, '_cname')
    assert.same('oop.example.MyClass', classname)
    assert.same('oop.example.MyClass', instance._cname)
    assert.same(nil, rawget(instance, '_cname'))
    assert.same(nil, instance._open)
    instance:open()
    assert.same(true, instance._open)
    instance:close()
    assert.same(false, instance._open)
  end)


  it("case1: subscribe to specific instance of class", function()
    local mod, log = {}, {}

    mod.handler = function(instance0, ...)
      local ts = tostring
      log[#log + 1] = string.format("handler: obj-id:%s args:%s first:%s",
        ts((instance0 or {}).id), ts(select('#', ...)), ts(select(1, ...) or nil)
      )
      bus.unsubscribe(instance0, 'close', mod.handler)
    end
    ----
    local instance_1 = MyClass:new({ id = 1 })
    local instance_2 = MyClass:new({ id = 2 })
    instance_1:open()
    instance_2:open()

    -- subscribe only to instance_1
    bus.subscribe(instance_1, 'close', mod.handler, true)

    -- before work:
    local exp = {
      [instance_1] = {
        ['close'] = { [mod.handler] = true, }
      }
    }
    assert.same(exp, bus._get_listeners())

    instance_2:close()
    assert.same({}, log) -- the handler does not starts
    assert.same(exp, bus._get_listeners())

    instance_1:close('event-data') -- inside: bus.fire_event(instance, 'close')

    -- after work:
    assert.same({}, bus._get_listeners())
    assert.same({ 'handler: obj-id:1 args:1 first:event-data' }, log)
  end)


  it("case1: subscribe to specific instance of class + autoclean", function()
    local mod, log = {}, {}

    mod.handler = function(obj, ...)
      local ts = tostring
      log[#log + 1] = string.format("handler: obj-id:%s args:%s first:%s",
        ts((obj or {}).id), ts(select('#', ...)), ts(select(1, ...) or nil)
      )
      bus.unsubscribe(obj, 'close', mod.handler) -- autoclean self from the bus
    end

    local instance_1 = MyClass:new({ id = 1 })
    local instance_2 = MyClass:new({ id = 2 })
    local instance_3 = MyClass:new({ id = 3 })

    instance_1:open()
    instance_2:open()
    instance_3:open()

    -- subscribe only to instance_1
    bus.subscribe(instance_1, 'close', mod.handler)
    bus.subscribe(instance_2, 'close', mod.handler)

    -- before work:
    local exp_bus = {
      [instance_1] = { ['close'] = { [mod.handler] = true, } },
      [instance_2] = { ['close'] = { [mod.handler] = true, } },
    }
    assert.same(exp_bus, bus._get_listeners())

    instance_3:close() -- the handler does not starts
    assert.same({}, log)

    instance_2:close('a', 'b') -- inside: bus.fire_event(instance, 'close')
    assert.same({ 'handler: obj-id:2 args:2 first:a' }, log)
    local exp_bus2 = {
      [instance_1] = { ['close'] = { [mod.handler] = true, } },
    }
    assert.same(exp_bus2, bus._get_listeners())

    instance_1:close()
    assert.same({}, bus._get_listeners())
    local exp = {
      'handler: obj-id:2 args:2 first:a',
      'handler: obj-id:1 args:0 first:nil'
    }
    assert.same(exp, log)
  end)


  it("case2: subscribe to all instances of one class", function()
    local mod, log = {}, {}

    mod.handler = function(obj, ...)
      local ts = tostring
      log[#log + 1] = string.format("handler: obj-id:%s args:%s first:%s",
        ts((obj or {}).id), ts(select('#', ...)), ts(select(1, ...) or nil)
      )
    end

    local instance_1 = MyClass:new({ id = 1 })
    local instance_2 = MyClass:new({ id = 2 })

    instance_1:open()
    instance_2:open()
    -- subscribe for all(*) instances
    bus.subscribe('*', 'close', mod.handler)

    -- before work:
    local exp_bus = {
      ['*'] = { ['close'] = { [mod.handler] = true, } },
    }
    assert.same(exp_bus, bus._get_listeners())

    instance_2:close('data')
    assert.same({ 'handler: obj-id:2 args:1 first:data' }, log)

    instance_1:close()

    local exp = {
      'handler: obj-id:2 args:1 first:data',
      'handler: obj-id:1 args:0 first:nil'
    }
    assert.same(exp, log)
    assert.same(exp_bus, bus._get_listeners())
  end)


  -- here combining usage of the bus:
  -- 1) create a regual(common) subscribtion for all instances of a class
  -- ( via '*' insted instance itself but retaining the ability to access the
  -- object that caused the event.
  -- 2) on-shoot handler for certain instance (with autoremove own subscribtion)
  it("case2+case1: subscribe to all and for one certain instance", function()
    local mod, log = {}, {}
    local ts, fmt = tostring, string.format

    mod.handler = function(obj) -- for all instances
      log[#log + 1] = fmt("handler-common(1): obj-id:%s", ts((obj or {}).id))
    end

    mod.handler2 = function(obj) -- for certain instance
      log[#log + 1] = fmt("handler-certain(2): obj-id:%s", ts((obj or {}).id))
      -- bus.unsubscribe(obj, 'close', mod.handler2) -- autoclean self from the bus
      bus.free(obj) -- remove all subscribed handlers for this object
      -- useful for a handler that is assigned to a method that marks an object
      -- as expired. that is, which must be removed from memory
    end

    local instance_1 = MyClass:new({ id = 1 })
    local instance_2 = MyClass:new({ id = 2 })

    instance_1:open()
    instance_2:open()
    bus.subscribe('*', 'close', mod.handler)         -- subscribe for all(*)
    bus.subscribe(instance_2, 'close', mod.handler2) -- for cernain instance

    -- before work:
    local exp_bus = {
      ['*'] = { ['close'] = { [mod.handler] = true, } },
      [instance_2] = { ['close'] = { [mod.handler2] = true, } },
    }
    assert.same(exp_bus, bus._get_listeners())

    instance_2:close('data')
    assert.same({ 'handler-certain(2): obj-id:2' }, log)

    instance_1:close()

    local exp = {
      'handler-certain(2): obj-id:2',
      'handler-common(1): obj-id:1'
    }
    -- notice: certain handler overrided the common handler for instance-2
    assert.same(exp, log)
    local exp_bus2 = {
      ['*'] = { ['close'] = { [mod.handler] = true, } },
      -- handler-2 automatically removed itself from the subscription after it
      -- was called
    }
    assert.same(exp_bus2, bus._get_listeners())
  end)
end)
