-- 11-02-2024 @author Swarg
-- Example to subscribe to one unique instance
-- to run use:
-- lua doc/examples.lua
--
--


local eventbus = require 'alib.eventbus'
local bus = eventbus.new()

--------------------------------------------------------------------------------

local MyClass = {
  _cname = 'app.MyClass',
  _open = nil,
}

--- Constructor
function MyClass:new(cdef) -- function Object.new(Object, o)
  cdef = cdef or {}
  self.__index = self      -- Object.__index = Object
  setmetatable(cdef, self)
  return cdef
end

function MyClass:open()
  self._open = true
  bus.fire_event(self, 'open') -- << call subscribed handlers
  --                    ^ event name
end

function MyClass:close(...)
  self._open = false
  bus.fire_event(self, 'close', ...) -- << call subscribed handlers
  --                             ^ event data for handler
end

--------------------------------------------------------------------------------
local M = {} -- module with handlers


M.handler_on_close = function(obj)
  print('handler_on_close fired for obj.id:', obj.id)
  bus.unsubscribe(obj, 'close', M.handler_on_close)
end

M.handler_on_close_special = function(obj)
  print('[#>handler_on_close_special<#] fired for obj.id:', obj.id)
  bus.unsubscribe(obj, 'close', M.handler_on_close)
end
-- return M

--------------------------------------------------------------------------------

local function v1_unique_handler()
  print("v1_unique_handler")
  local instance_1 = MyClass:new({ id = 1 })
  local instance_2 = MyClass:new({ id = 2 })
  local instance_3 = MyClass:new({ id = 3 })
  instance_1:open()
  instance_2:open()
  instance_3:open()

  bus.subscribe(instance_1, 'close', M.handler_on_close, true)

  instance_1:close()
  instance_2:close()
  instance_3:close()
end


local function v2_subscribe_to_all()
  print("v2_subscribe_to_all")
  local instance_1 = MyClass:new({ id = 1 })
  local instance_2 = MyClass:new({ id = 2 })
  local instance_3 = MyClass:new({ id = 3 })
  instance_1:open()
  instance_2:open()
  instance_3:open()

  bus.subscribe('*', 'close', M.handler_on_close, true)

  instance_1:close()
  instance_2:close()
  instance_3:close()
end


local function v3_subscribe_to_all_with_overriding()
  print("v3_subscribe_to_all_with_overriding")
  local instance_1 = MyClass:new({ id = 1 })
  local instance_2 = MyClass:new({ id = 2 })
  local instance_3 = MyClass:new({ id = 3 })
  instance_1:open()
  instance_2:open()
  instance_3:open()

  bus.subscribe('*', 'close', M.handler_on_close, true)
  bus.subscribe(instance_3, 'close', M.handler_on_close_special, true)

  instance_1:close()
  instance_2:close()
  instance_3:close()
end

local i = tonumber(arg[1] or 1)

if i == 1 then
  v1_unique_handler()
elseif i == 2 then
  v2_subscribe_to_all()
elseif i == 3 then
  v3_subscribe_to_all_with_overriding()
end
